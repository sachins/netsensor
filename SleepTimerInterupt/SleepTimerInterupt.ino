#include <avr/sleep.h>
#include <avr/power.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define DHT11_PIN 4

int counter = 0;
float sleep_time_min = 0.2; //sleep time in minutes
//float float_count = (sleep_time_min * 60) / 4.19;
float float_count = (sleep_time_min * 60) / 0.01632;
int sleep_time_count = (int)float_count; //roughly, not precisely
int display_num = 0;
int pinLed=13;



void do_some_work()
{
  /*
  DHT.read11(DHT11_PIN);
  client.print(DHT.temperature);      
    Serial.print("Temperature: ");
    Serial.print(DHT.temperature);
    Serial.print("\r\n");   
   delay(200); 
   */
   /*
    Serial.println("woke up!!\r\n");
    delay(200);
    Serial.print("Sleep time count is: ");
    Serial.print(sleep_time_count);
    Serial.print("\r\n");    
    */
  pinMode(pinLed,OUTPUT);
  digitalWrite(pinLed, digitalRead(pinLed) ^ 1);
  //digitalWrite(pinLed, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(300);  

  
}
void setup() {
  
  // initialize timer1 
  noInterrupts();           // disable all interrupts
  TCCR2A = 0;
  TCCR2B = 0;

  TCNT2 = 0;            // preload timer 
  //TCCR1B = 5;
  //TCCR1B |= (1 << CS12 || 1 << CS10);    // 1024 prescaler 
 //TCCR1B |= (1 << CS12 );
  TCCR2B = 7; // 1024 prescaler
  TIMSK2 |= (1 << TOIE2);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts  
  
  //Serial.begin(38400);
}

void loop()
{
  //Serial.println("sleep..\r\n");
  //delay(100);
  //do_some_work();
  system_sleep();  
}


//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {
/* Now is the time to set the sleep mode. In the Atmega8 datasheet
     * http://www.atmel.com/dyn/resources/prod_documents/doc2486.pdf on page 35
     * there is a list of sleep modes which explains which clocks and 
     * wake up sources are available in which sleep modus.
     *
     * In the avr/sleep.h file, the call names of these sleep modus are to be found:
     *
     * The 5 different modes are:
     *     SLEEP_MODE_IDLE         -the least power savings 
     *     SLEEP_MODE_ADC
     *     SLEEP_MODE_PWR_SAVE
     *     SLEEP_MODE_STANDBY
     *     SLEEP_MODE_PWR_DOWN     -the most power savings
     *
     *  the power reduction management <avr/power.h>  is described in 
     *  http://www.nongnu.org/avr-libc/user-manual/group__avr__power.html
     */  
  cbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_IDLE);
//  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here

  sleep_enable();          // enables the sleep bit in the mcucr register
                             // so sleep is possible. just a safety pin 
  
  power_adc_disable(); //sensor use adc
  power_spi_disable(); //ethernet shield uses spi
  power_timer0_disable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  //power_timer2_disable();
  power_twi_disable();

  sleep_mode();                        // System sleeps here
}

void system_wake_up()
{
  sleep_disable();                     // System continues execution here when watchdog timed out 
  //sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON  
  power_adc_enable(); //sensor use adc
  power_spi_enable(); //ethernet shield uses spi
  power_timer0_enable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  //power_timer2_enable();
  //power_twi_disable();  
}

ISR(TIMER2_OVF_vect)        // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  if (counter < sleep_time_count)
  {
    counter++;
  }
  else
  {
    system_wake_up();
    counter=0;
    //do nothing, just wake up the arduino.
    do_some_work(); 
  }
}
