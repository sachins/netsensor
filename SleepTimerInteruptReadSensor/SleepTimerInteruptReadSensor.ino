#include <avr/sleep.h>
#include <avr/power.h>
#include <OneWire.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

//#define DHT11_PIN 4
int tempPwrpin = 5;
int DS18S20_Pin = 6; //DS18S20 Signal pin on digital 2
int tempGNDpin = 7;

int counter = 0;
float sleep_time_min = 0.05; //sleep time in minutes
//float float_count = (sleep_time_min * 60) / 4.19;
float float_count = (sleep_time_min * 60) / 0.01632;
int sleep_time_count = (int)float_count; //roughly, not precisely
int display_num = 0;
int pinLed=13;

OneWire ds(DS18S20_Pin);  
//uint8_t sensor_address[8];
char sensor_address[8];

void do_some_work()
{
  /*
  DHT.read11(DHT11_PIN);
  client.print(DHT.temperature);      
    Serial.print("Temperature: ");
    Serial.print(DHT.temperature);
    Serial.print("\r\n");   
   delay(200); 
   */
   /*
    Serial.println("woke up!!\r\n");
    delay(200);
    Serial.print("Sleep time count is: ");
    Serial.print(sleep_time_count);
    Serial.print("\r\n");    
    */
//  float temperature = getTemp();
  float temperature = readValidTemp();
  Serial.println(temperature); 
  
  delay(500); //just here to slow down the output so it is easier to read
      
  pinMode(pinLed,OUTPUT);
  digitalWrite(pinLed, digitalRead(pinLed) ^ 1);
  //digitalWrite(pinLed, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(300);  

  
}
void SensorEnable()
{ 
  ds.reset(); 
  pinMode(tempPwrpin, OUTPUT);
  pinMode(tempGNDpin, OUTPUT);  
  digitalWrite(tempGNDpin, LOW);
  digitalWrite(tempPwrpin, HIGH);  
}

void SensorDisable()
{
   ds.depower(); //sensor data pin power down.
   pinMode(tempPwrpin, INPUT);
   pinMode(tempGNDpin, INPUT); 
   digitalWrite(tempGNDpin, LOW);
   digitalWrite(tempPwrpin, LOW); 
}
float getTemp(){
  //returns the temperature from one DS18S20 in DEG Celsius

  byte data[12];
  byte addr[8];

  if ( !ds.search(addr)) {
      //no more sensors on chain, reset search
      ds.reset_search();
      return -1000;
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return -1000;
  }

  if ( addr[0] != 0x10 && addr[0] != 0x28) {
      Serial.print("Device is not recognized");
      return -1000;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end

  byte present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); // Read Scratchpad

  
  for (int i = 0; i < 9; i++) { // we need 9 bytes
    data[i] = ds.read();
  }
  
  ds.reset_search();
  
  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TemperatureSum = tempRead / 16;
  
  return TemperatureSum;
  
}
float readValidTemp()
{
  int max_n_attempt = 10;
  float temperature;
  do
  {
    temperature = getTemp();  
  }
  while(temperature == -1000 || (int)temperature == 85);
  return temperature;
}  
void setup() {
  
  // initialize timer1 
  noInterrupts();           // disable all interrupts
  TCCR2A = 0;
  TCCR2B = 0;

  TCNT2 = 0;            // preload timer 
  //TCCR1B = 5;
  //TCCR1B |= (1 << CS12 || 1 << CS10);    // 1024 prescaler 
 //TCCR1B |= (1 << CS12 );
  TCCR2B = 7; // 1024 prescaler
  TIMSK2 |= (1 << TOIE2);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts  
  SensorEnable();
  Serial.begin(38400); 
  
  //ds.getAddress(sensor_address);
  //Serial.println(sensor_address, HEX);
  unsigned int address = ds.getAddress();
  Serial.println(address);
  //char *cc = (char) sensor_address;
//address = atoi(sensor_address);
//  address = atoi(cc);
  Serial.println(address);
  /*
  for (int i=0;i<8;i++)
  {
    Serial.print(sensor_address[i]);
  }  
  Serial.println();     
  */
}

void loop()
{
  //Serial.println("sleep..\r\n");
  //delay(100);
  //do_some_work();
  system_sleep();  
}


//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {
  interrupts();             // enable all interrupts
  SensorDisable();  
/* Now is the time to set the sleep mode. In the Atmega8 datasheet
     * http://www.atmel.com/dyn/resources/prod_documents/doc2486.pdf on page 35
     * there is a list of sleep modes which explains which clocks and 
     * wake up sources are available in which sleep modus.
     *
     * In the avr/sleep.h file, the call names of these sleep modus are to be found:
     *
     * The 5 different modes are:
     *     SLEEP_MODE_IDLE         -the least power savings 
     *     SLEEP_MODE_ADC
     *     SLEEP_MODE_PWR_SAVE
     *     SLEEP_MODE_STANDBY
     *     SLEEP_MODE_PWR_DOWN     -the most power savings
     *
     *  the power reduction management <avr/power.h>  is described in 
     *  http://www.nongnu.org/avr-libc/user-manual/group__avr__power.html
     */  
  cbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_IDLE);
//  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here

  sleep_enable();          // enables the sleep bit in the mcucr register
                             // so sleep is possible. just a safety pin 
  
  power_adc_disable(); //sensor use adc
  power_spi_disable(); //ethernet shield uses spi
  //power_timer0_disable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  //power_timer2_disable();
  power_twi_disable();
  //should reset timer her
  sbi(TIMSK2,TOIE2); //Enable overflow interrupt
  sleep_mode();                        // System sleeps here
}

void system_wake_up()
{
  
  sleep_disable();                     // System continues execution here when watchdog timed out 
  //sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON  
  power_adc_enable(); //sensor use adc
  power_spi_enable(); //ethernet shield uses spi
  power_timer0_enable();
  power_timer1_enable(); //timer1 overflow interupt is in use
  power_timer2_enable();
  //power_twi_enable();  
  noInterrupts();           // disable all interrupts
  SensorEnable();
}

ISR(TIMER2_OVF_vect)        // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  if (counter < sleep_time_count)
  {
    counter++;
  }
  else
  {
    system_wake_up();
    counter=0;
    //do nothing, just wake up the arduino.
    do_some_work(); 
  }
}
