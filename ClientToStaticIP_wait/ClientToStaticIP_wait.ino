/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 18 Dec 2009
 modified 9 Apr 2012
 by David A. Mellis
 
 */

#include <SPI.h>
#include <Ethernet.h>
#include <dht.h>
#define DHT11_PIN 4
#include <avr/sleep.h>
#include <avr/wdt.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

int counter = 0;
float sleep_time_min = 0.05; //sleep time in minutes
float float_count = (sleep_time_min * 60) / 8;
int sleep_time_count = (int)float_count; //roughly, not precisely
int display_num = 0;
//int pinLed=13;
dht DHT;


// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//IPAddress server(173,194,70,113); // Google
IPAddress server(119,47,122,253); // Tim's server

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void do_some_work()
{
  /*
  DHT.read11(DHT11_PIN);
  client.print(DHT.temperature);      
    Serial.print("Temperature: ");
    Serial.print(DHT.temperature);
    Serial.print("\r\n");   
   delay(200); 
   */
   /*
    Serial.println("woke up!!\r\n");
    delay(200);
    Serial.print("Sleep time count is: ");
    Serial.print(sleep_time_count);
    Serial.print("\r\n");    
    */
  //pinMode(pinLed,OUTPUT);
  //digitalWrite(pinLed, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(200);
  
   // Open serial communications and wait for port to open:
  //Serial.begin(9600);
  // wait for serial port to connect. Needed for Leonardo only
 // delay(100);
  // start the Ethernet connection:
  /*
  if (Ethernet.begin(mac) == 0) {
    //Serial.println("Failed to configure Ethernet using DHCP");
  } 
 */ 
  // give the Ethernet shield a second to initialize:
  //delay(1000);  
  // if you get a connection, report back via serial:
  if (client.connect(server, 80)) {
    Serial.println("connected");
    //DHT.read11(DHT11_PIN);
    // Make a HTTP request:
    client.print("GET http://porters.host1.dodat.co.nz/temp.php?temp=");
    //client.print(DHT.temperature);
    client.print(display_num);
    client.print("&id=6");
    client.print("&index=12");      
    client.println(" HTTP/1.1"); 
    //client.println("GET http://porters.host1.dodat.co.nz/temp.php?temp=221 HTTP/1.1");
    client.println("Host: porters.host1.dodat.co.nz");
    client.println();//add a space at the end
    client.stop();
  } 
  else {
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
  }  
  
}
void setup() {
  Serial.begin(38400);
Ethernet.begin(mac);
  // give the Ethernet shield a second to initialize:
  delay(1000);  
}

void loop()
{
  //Serial.println("sleep..\r\n");
  //delay(100);

    display_num++;
    do_some_work();
    delay(10000);
    //delay(300000);
}


//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {

  cbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  sleep_enable();

  sleep_mode();                        // System sleeps here

    //sleep_disable();                     // System continues execution here when watchdog timed out 
    //sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON

}
//****************************************************************
// 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
// 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void setup_watchdog(int ii) {

  byte bb;
  int ww;
  if (ii > 9 ) ii=9; //8 sec maximum;
  bb=ii & 7;
  if (ii > 7) bb|= (1<<5);
  bb|= (1<<WDCE);
  ww=bb;
  //Serial.println(ww);


  MCUSR &= ~(1<<WDRF);
  // start timed sequence
  WDTCSR |= (1<<WDCE) | (1<<WDE);
  // set new watchdog timeout value
  WDTCSR = bb;
  WDTCSR |= _BV(WDIE);
}
//****************************************************************  
// Watchdog Interrupt Service / is executed when  watchdog timed out
ISR(WDT_vect) {
  if (counter < sleep_time_count)
  {
    counter++; 
    /*
    Serial.print("Counter is: ");
    Serial.print(counter);
    Serial.print("\r\n");   
    Serial.print("Sleep time count is: ");
    Serial.print(sleep_time_count);
    Serial.print("\r\n");   
    */
  }
  else
  {
    //reset counter
    counter=0;
    sleep_disable();                     // System continues execution here when watchdog timed out 
    sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON    
    display_num++;
    do_some_work();
  }
}

