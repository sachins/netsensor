/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 18 Dec 2009
 modified 9 Apr 2012
 by David A. Mellis
 
 */

#include <SPI.h>
#include <Ethernet.h>
#include <dht.h>
#define DHT11_PIN 4
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

int counter = 0;
float sleep_time_min = 5; //sleep time in minutes
float float_count = (sleep_time_min * 60) / 8;
int sleep_time_count = (int)float_count; //roughly, not precisely
int display_num = 0;
//int pinLed=13;
dht DHT;


// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//IPAddress server(173,194,70,113); // Google
IPAddress server(119,47,122,253); // Tim's server

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void do_some_work()
{
   display_num++;
  /*
  DHT.read11(DHT11_PIN);
  client.print(DHT.temperature);      
    Serial.print("Temperature: ");
    Serial.print(DHT.temperature);
    Serial.print("\r\n");   
   delay(200); 
   */
   /*
    Serial.println("woke up!!\r\n");
    delay(200);
    Serial.print("Sleep time count is: ");
    Serial.print(sleep_time_count);
    Serial.print("\r\n");    
    */
  //pinMode(pinLed,OUTPUT);
  //digitalWrite(pinLed, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(200);
  
   // Open serial communications and wait for port to open:
  //Serial.begin(9600);
  // wait for serial port to connect. Needed for Leonardo only
 // delay(100);
  // start the Ethernet connection:
  /*
  if (Ethernet.begin(mac) == 0) {
    //Serial.println("Failed to configure Ethernet using DHCP");
  } 
 */ 
  // give the Ethernet shield a second to initialize:
  //delay(1000);  
  // if you get a connection, report back via serial:
  if (client.connect(server, 80)) {
    //Serial.println("connected");
    //DHT.read11(DHT11_PIN);
    // Make a HTTP request:
    client.print("GET http://porters.host1.dodat.co.nz/temp.php?temp=");
    //client.print(DHT.temperature);
    client.print(display_num);
    client.println(" HTTP/1.1"); 
    //client.println("GET http://porters.host1.dodat.co.nz/temp.php?temp=221 HTTP/1.1");
    client.println("Host: porters.host1.dodat.co.nz");
    client.println();//add a space at the end
    client.stop();
  } 
  else {
    // kf you didn't get a connection to the server:
    //Serial.println("connection failed");
  }  
  
}
//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {
/* Now is the time to set the sleep mode. In the Atmega8 datasheet
     * http://www.atmel.com/dyn/resources/prod_documents/doc2486.pdf on page 35
     * there is a list of sleep modes which explains which clocks and 
     * wake up sources are available in which sleep modus.
     *
     * In the avr/sleep.h file, the call names of these sleep modus are to be found:
     *
     * The 5 different modes are:
     *     SLEEP_MODE_IDLE         -the least power savings 
     *     SLEEP_MODE_ADC
     *     SLEEP_MODE_PWR_SAVE
     *     SLEEP_MODE_STANDBY
     *     SLEEP_MODE_PWR_DOWN     -the most power savings
     *
     *  the power reduction management <avr/power.h>  is described in 
     *  http://www.nongnu.org/avr-libc/user-manual/group__avr__power.html
     */  
  cbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_IDLE);
//  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here

  sleep_enable();          // enables the sleep bit in the mcucr register
                             // so sleep is possible. just a safety pin 
  
  power_adc_disable(); //sensor use adc
  power_spi_disable(); //ethernet shield uses spi
  power_timer0_disable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  power_timer2_disable();
  power_twi_disable();

  sleep_mode();                        // System sleeps here
}

//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_wake_up()
{
  sleep_disable();                     // System continues execution here when watchdog timed out 
  //sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON  
  power_adc_enable(); //sensor use adc
  power_spi_enable(); //ethernet shield uses spi
  power_timer0_enable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  power_timer2_enable();
  //power_twi_disable();  
}

ISR(TIMER1_OVF_vect)        // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  if (counter < sleep_time_count)
  {
    counter++;
  }
  else
  {
    system_wake_up();
    counter=0;
    //do nothing, just wake up the arduino.
    do_some_work(); 
  }
}

void setup() {
  //Serial.begin(38400);
  //in realy deployment may be a good idea to delay the arduino for at least 3 minutes for other devices to be powered up and initialised
  delay(60000);
Ethernet.begin(mac);
  // give the Ethernet shield a second to initialize:
  delay(1000);  

  // initialize timer1 
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = 0;            // preload timer 
  //TCCR1B = 5;
  TCCR1B |= (1 << CS12 | 1 << CS10);    // 1024 prescaler 
 //TCCR1B |= (1 << CS12 );
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts  
  
  //do some work before going to sleep
  do_some_work();
}

void loop()
{
  system_sleep();    
}



