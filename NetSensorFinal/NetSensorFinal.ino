/*
  Net sensor
  
  Developed by 
  Hobbyist Ltd
  
  Created 24 06 2012
  Modified 24 06 2012
  By King Au
 */

#include <SPI.h>
#include <Ethernet.h>
#include <OneWire.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define TEMP_PWR_PIN 5
#define TEMP_DATA_PIN 6 
#define TEMP_GND_PIN 7

int counter = 0;
float sleep_time_min = 5; //sleep time in minutes
float float_count = (sleep_time_min * 60) / 4.19; //65535/(16Mhz/1024)=4.19
int sleep_time_count = (int)float_count; //roughly, not precisely
int sample_index = 0;
//int pinLed=13;


// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC addess printed on a sticker on the shield
byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//IPAddress server(173,194,70,113); // Google
IPAddress server(119,47,122,253); // Tim's server
OneWire ds(TEMP_DATA_PIN);
//OneWire ds(DS18S20_Pin); 
//uint8_t sensor_address[8]; //int array
unsigned int address; //a single int, but only 16 bits

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void do_some_work()
{
//   float temperature = getTemp();  
  
   float temperature = readValidTemp();   
  // if you get a connection, report back via serial:
  if (client.connect(server, 80)) {
    //Serial.println("connected");
    //DHT.read11(DHT11_PIN);
    // Make a HTTP request:
    client.print("GET http://porters.host1.dodat.co.nz/temp.php?temp=");
    client.print(temperature);
    //client.print("&id=6");
    //client.print("&index=12");    
    client.print("&id=");
    client.print(address);
    client.print("&index=");
    client.print(sample_index);    
    client.println(" HTTP/1.1"); 
    //client.println("GET http://porters.host1.dodat.co.nz/temp.php?temp=221 HTTP/1.1");
    client.println("Host: porters.host1.dodat.co.nz");
    client.println();//add a space at the end
    client.stop();
  } 
  else {
    // kf you didn't get a connection to the server:
    //Serial.println("connection failed");
  }  
  sample_index++;
}
void SensorEnable()
{ 
  ds.reset(); 
  pinMode(TEMP_GND_PIN, OUTPUT);
  pinMode(TEMP_PWR_PIN, OUTPUT);  
  digitalWrite(TEMP_GND_PIN, LOW);
  digitalWrite(TEMP_PWR_PIN, HIGH);   
  delay(1000);
}

void SensorDisable()
{
   ds.depower(); //sensor data pin power down.
   pinMode(TEMP_GND_PIN, INPUT);
   pinMode(TEMP_PWR_PIN, INPUT); 
   digitalWrite(TEMP_GND_PIN, LOW);
   digitalWrite(TEMP_PWR_PIN, LOW); 
}
float getTemp(){
  //returns the temperature from one DS18S20 in DEG Celsius

  byte data[12];
  byte addr[8];

  if ( !ds.search(addr)) {
      //no more sensors on chain, reset search
      ds.reset_search();
      return -1000;
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      //Serial.println("CRC is not valid!");
      return -1000;
  }

  if ( addr[0] != 0x10 && addr[0] != 0x28) {
//      Serial.print("Device is not recognized");
      return -1000;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end

  byte present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); // Read Scratchpad

  
  for (int i = 0; i < 9; i++) { // we need 9 bytes
    data[i] = ds.read();
  }
  
  ds.reset_search();
  
  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TemperatureSum = tempRead / 16;
  
  return TemperatureSum;
  
}
float readValidTemp()
{
  int max_n_attempt = 10;
  float temperature;
  do
  {
    temperature = getTemp();  
  }
  while(temperature == -1000 || (int)temperature == 85);
  return temperature;
}  
//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {
  interrupts();             // enable all interrupts
  SensorDisable(); 
/* Now is the time to set the sleep mode. In the Atmega8 datasheet
     * http://www.atmel.com/dyn/resources/prod_documents/doc2486.pdf on page 35
     * there is a list of sleep modes which explains which clocks and 
     * wake up sources are available in which sleep modus.
     *
     * In the avr/sleep.h file, the call names of these sleep modus are to be found:
     *
     * The 5 different modes are:
     *     SLEEP_MODE_IDLE         -the least power savings 
     *     SLEEP_MODE_ADC
     *     SLEEP_MODE_PWR_SAVE
     *     SLEEP_MODE_STANDBY
     *     SLEEP_MODE_PWR_DOWN     -the most power savings
     *
     *  the power reduction management <avr/power.h>  is described in 
     *  http://www.nongnu.org/avr-libc/user-manual/group__avr__power.html
     */  
  cbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_IDLE);
//  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here

  sleep_enable();          // enables the sleep bit in the mcucr register
                             // so sleep is possible. just a safety pin 
  
  power_adc_disable(); //sensor use adc
  power_spi_disable(); //ethernet shield uses spi
  power_timer0_disable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  power_timer2_disable();
  power_twi_disable();

  sleep_mode();                        // System sleeps here
}

//****************************************************************  
// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_wake_up()
{
  sleep_disable();                     // System continues execution here when watchdog timed out 
  //sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON  
  power_adc_enable(); //sensor use adc
  power_spi_enable(); //ethernet shield uses spi
  power_timer0_enable();
  //power_timer1_disable(); //timer1 overflow interupt is in use
  power_timer2_enable();
  //power_twi_disable();  
  noInterrupts();           // disable all interrupts
  SensorEnable();
}

ISR(TIMER1_OVF_vect)        // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  if (counter < sleep_time_count)
  {
    counter++;
    //back to sleep
  }
  else
  {
    system_wake_up();
    counter=0;
    //do nothing, just wake up the arduino.
    do_some_work(); 
  }
}  
void setup() {


  //Serial.begin(38400);  
  //in realy deployment may be a good idea to delay the arduino for at least 3 minutes for other devices to be powered up and initialised
  delay(300000);
  //delay(10000);
  while (Ethernet.begin(mac) == 0) {
    delay(1000);
    //maybe the router is not turned on.
//    Serial.println("Failed to configure Ethernet using DHCP");
  }

  // give the Ethernet shield a second to initialize:
  delay(1000);  

  // initialize timer1 
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;            // preload timer 
  //TCCR1B = 5;
  TCCR1B |= (1 << CS12 | 1 << CS10);    // 1024 prescaler 
 //TCCR1B |= (1 << CS12 );
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
    
     SensorEnable();
     delay(1000);
  //get the sensor address
//  ds.getAddress(sensor_address);
  address = ds.getAddress();
    //Serial.print(address); 
  //do some work before going to sleep
  do_some_work();
  interrupts();             // enable all interrupts  
}

void loop()
{
  system_sleep();    
}



